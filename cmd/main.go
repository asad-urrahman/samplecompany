package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"samplecompany/pkg/dao"
	"samplecompany/pkg/router"

	"github.com/gorilla/handlers"
)

func main() {
	port := flag.Int("port", 8081, "Server port")
	flag.Parse()

	url := fmt.Sprintf("127.0.0.1:%d", *port)

	dbname := "test.db"

	db, err := dao.New(dbname)
	if err != nil {
		log.Fatalf("DB init failed: %v\n", err)
	}

	handler := router.New(db)

	log.Printf("Listening on: %v\n", url)

	// Add logging middleware
	err = http.ListenAndServe(url, handlers.LoggingHandler(os.Stdout, handler))
	log.Fatalf("Server closed: %v\n", err)
}
