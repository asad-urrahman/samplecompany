# SampleCompany

## Initial Design:
- Define Project structure
    - Contains main pkg (executables) `cmd`
    - DB interactions `dao`
    - Web routers `router`
    - Tests `tests`
- Database selection: SQLlite for simplicity
- GORM for database models
    - From task description; only one `computers` table is needed no explicit requirement for `employee` table
- I will use gorilla mux (though it is archived but still mature and have used extensively in past)
- Implementation of Database CRUD operations interface
- Implementation of web router

## Test:
```
go test -v ./...
```

## RUN:
```
cd cmd

go run -v . -port=9090

OR

go build -o samplecompany .
./samplecompany -port=9090
```

## TODO:

1. Unify test cases for the DB and router
2. Resolve all TODOs/FIXMEs
3. `gorilla/mux` is archived may use alternative
4. Use proper logging
5. Document API endpoints OpenAPI / swagger
6. SQLite is not scalable
7. Redesign DB Scheme (multiple tables: computers, employees ...)
8. Secure API endpoints (TLS only, auth, rate limit ...)
9. Validate inputs and content-types
10. Benchmarking
