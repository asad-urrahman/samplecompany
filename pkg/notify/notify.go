package notify

import (
	"encoding/json"
	"fmt"
	"log"
)

type Notifier struct {
	tr Transporter
}

type Message struct {
	Level                string
	EmployeeAbbreviation string
	Message              string
}

type Transporter interface {
	Send(jsonData []byte) error
}

func NewNotifier(url string) *Notifier {
	return &Notifier{tr: &HTTPTransporter{url: url}}
}

func (n *Notifier) Warnf(abbr, msg string, args ...any) error {
	return n.notify("warning", abbr, msg, args...)
}

func (n *Notifier) SetTransporter(tr Transporter) {
	n.tr = tr
}

func (n *Notifier) notify(level, abbr, msg string, args ...any) error {
	obj := Message{
		Level: level, EmployeeAbbreviation: abbr,
		Message: fmt.Sprintf(msg, args...),
	}

	log.Printf("NOTIFIER: %+v\n", obj)

	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	err = n.tr.Send(data)
	return err
}
