package notify

import (
	"bytes"
	"fmt"
	"net/http"
)

type HTTPTransporter struct {
	url string
}

func (tr *HTTPTransporter) Send(jsonData []byte) error {
	resp, err := http.Post(tr.url, "application/json", bytes.NewReader(jsonData))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected HTTP status %v", resp.Status)
	}

	return nil
}
