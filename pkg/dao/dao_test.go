package dao

import (
	"testing"

	"samplecompany/pkg/dao/models"
	"samplecompany/pkg/testutils"

	"github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/require"
)

type testCase struct {
	Name    string
	C       models.Computer
	wantErr bool
}

func TestDAO(t *testing.T) {
	dbname := t.TempDir() + "/test.db"

	dao, err := New(dbname)
	require.NoError(t, err)

	numOfTestCases := 3
	testCaseComputers := testutils.GenFakeComputers(numOfTestCases)

	// TODO:
	// 1. Add test case for validating Abbreviation size constraint

	t.Run("Create", func(t *testing.T) {
		tcs := []testCase{
			{
				Name: "Computer-0",
				C:    testCaseComputers[0],
			},
			{
				Name: "Computer-1",
				C:    testCaseComputers[1],
			},
			{
				Name: "Computer-2",
				C:    testCaseComputers[2],
			},
			{
				// OPTIONAL field
				Name: "empty-Abbreviation",
				C:    testutils.GenFakeComputerWithField("Abbreviation", ""),
			},
			{
				// OPTIONAL field
				Name: "empty-Description",
				C:    testutils.GenFakeComputerWithField("Description", ""),
			},
			{
				Name:    "empty-MAC",
				C:       testutils.GenFakeComputerWithField("MAC", ""),
				wantErr: true, // NOT NULL CONSTRAINT
			},
			{
				Name:    "empty-Name",
				C:       testutils.GenFakeComputerWithField("Name", ""),
				wantErr: true, // NOT NULL CONSTRAINT
			},
			{
				Name:    "empty-IPAddress",
				C:       testutils.GenFakeComputerWithField("IPAddress", ""),
				wantErr: true, // NOT NULL CONSTRAINT
			},
			{
				Name:    "Re-Create-Computer-2",
				C:       testCaseComputers[2],
				wantErr: true,
			},
		}
		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				err = dao.CreateComputer(&tc.C)
				if tc.wantErr {
					require.Error(t, err)
					return
				}

				require.NoError(t, err)
			})

		}
	})

	t.Run("Get", func(t *testing.T) {
		tcs := []testCase{
			{
				Name: "Computer-0",
				C:    testCaseComputers[0],
			},
			{
				Name: "Computer-1",
				C:    testCaseComputers[1],
			},
			{
				Name: "Computer-2",
				C:    testCaseComputers[2],
			},
			{
				Name:    "Non-existing",
				C:       testutils.GenFakeComputer(), // New object, does not exist in DB
				wantErr: true,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				record, err := dao.GetComputer(tc.C.MAC)
				if tc.wantErr {
					require.Error(t, err)
					return
				}

				require.NoError(t, err)
				require.Equal(t, &tc.C, record)
			})

		}

	})

	t.Run("GetAll", func(t *testing.T) {
		computers, err := dao.GetAllComputers()
		require.NoError(t, err)

		require.Len(t, computers, numOfTestCases+2) // 2 more for created with empty abbreviation and description
	})

	t.Run("Update", func(t *testing.T) {
		// TODO: Add more test cases for
		// 1. Update item which doest not exist in the DB
		// 2. Updated the REQUIRED field to empty

		for i := range testCaseComputers {
			// Update IP address
			testCaseComputers[i].IPAddress = gofakeit.IPv4Address()

			err := dao.UpdateComputer(&testCaseComputers[i])
			require.NoError(t, err)

			// Get back updated computer
			record, err := dao.GetComputer(testCaseComputers[i].MAC)
			require.NoError(t, err)
			require.Equal(t, &testCaseComputers[i], record)

		}
	})

	t.Run("Delete", func(t *testing.T) {
		// TODO: Add more test cases for
		// 1. Delete item which doest not exist in the DB

		for _, c := range testCaseComputers {
			err := dao.DeleteComputer(c.MAC)
			require.NoError(t, err)

			_, err = dao.GetComputer(c.MAC)
			require.Error(t, err) // should return error
		}
	})
}
