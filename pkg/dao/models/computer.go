package models

import (
	"log"

	"samplecompany/pkg/notify"

	"gorm.io/gorm"
)

// The system administrator needs to store the following elements for each computer: MAC
// address (required), computer name (required), IP address (required), employee abbreviation
// (optional) and description (optional). The employee abbreviation consists of 3 letters. For
// example Max Mustermann should be mmu.

// UNIQUE constraints ??
type Computer struct {
	// MAC address (required)
	MAC string `gorm:"primarykey;not null;default:null"`
	// Computer name (required)
	Name string `gorm:"not null;default:null"`
	// IP address (required)
	IPAddress string `gorm:"not null;default:null"`
	// Employee abbreviation (optional) fixed size of 3 letters.
	Abbreviation string `gorm:"size:3"`
	// Description (optional)
	Description string `gorm:""`
}

const NotificationUsersLimit = 3

var (
	Notifier = notify.NewNotifier("http://0.0.0.0:8080/api/notify")
)

func (c *Computer) AfterCreate(tx *gorm.DB) error {
	if c.Abbreviation == "" { // employee isn't assigned
		return nil
	}

	var count int64
	err := tx.Model(&Computer{}).Where("abbreviation= ?", c.Abbreviation).Count(&count).Error
	if err != nil {
		// TODO: add logger
		return err
	}

	if count < NotificationUsersLimit {
		return nil
	}

	err = Notifier.Warnf(c.Abbreviation, "Number of assigned computer (%d) exceeded limit (%d)", count, NotificationUsersLimit)
	if err != nil {
		// TODO: add logger
		log.Printf("Notification failed: %v\n", err)
	}

	return nil
}
