package dao

import (
	"samplecompany/pkg/dao/models"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DAO struct {
	db *gorm.DB
}

// New creates database access object
func New(dbname string) (*DAO, error) {
	db, err := gorm.Open(sqlite.Open(dbname), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	err = db.AutoMigrate(&models.Computer{})
	if err != nil {
		return nil, err
	}

	return &DAO{db: db}, nil
}

func (dao *DAO) CreateComputer(c *models.Computer) error {
	err := dao.db.Create(c).Error
	return err
}

func (dao *DAO) GetComputer(mac string) (*models.Computer, error) {
	c := &models.Computer{MAC: mac} // mac is primary key
	err := dao.db.First(c).Error
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (dao *DAO) GetEmployeeComputers(abbr string) ([]models.Computer, error) {
	computers := []models.Computer{}
	err := dao.db.Where(&models.Computer{Abbreviation: abbr}).Find(&computers).Error
	if err != nil {
		return nil, err
	}

	return computers, nil
}

func (dao *DAO) GetAllComputers() ([]models.Computer, error) {
	computers := []models.Computer{}
	err := dao.db.Find(&computers).Error
	if err != nil {
		return nil, err
	}

	return computers, nil
}

func (dao *DAO) UpdateComputer(c *models.Computer) error {
	err := dao.db.Model(&models.Computer{MAC: c.MAC}).Updates(c).Error // update all non-zero columns
	return err
}

func (dao *DAO) DeleteComputer(mac string) error {
	err := dao.db.Delete(&models.Computer{MAC: mac}).Error
	return err
}

func (dao *DAO) RemoveEmployeeComputer(abbr, mac string) error {
	// Clear the employee reference (abbreviation), deleting computer object from DB isnt needed
	c := &models.Computer{MAC: mac, Abbreviation: abbr}
	err := dao.db.Model(c).Update("abbreviation", "").Error
	return err
}

func (dao *DAO) AssignEmployeeComputer(abbr, mac string) error {
	// Update the employee reference (abbreviation)
	c := &models.Computer{MAC: mac}
	err := dao.db.Model(c).Update("abbreviation", abbr).Error
	return err
}
