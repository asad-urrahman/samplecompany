package testutils

import (
	"reflect"

	"samplecompany/pkg/dao/models"

	"github.com/brianvoe/gofakeit"
)

func GenFakeComputers(n int) []models.Computer {
	computers := []models.Computer{}

	for i := 0; i < n; i++ {

		computers = append(computers, GenFakeComputer())
	}

	return computers
}

func GenFakeComputer() models.Computer {
	c := models.Computer{
		MAC:          gofakeit.MacAddress(),
		Name:         gofakeit.Name(),
		IPAddress:    gofakeit.IPv4Address(),
		Abbreviation: gofakeit.Lexify("???"),
		Description:  gofakeit.HackerPhrase(),
	}

	return c
}

func GenFakeComputerWithField(fieldName string, value interface{}) models.Computer {
	c := GenFakeComputer()

	reflect.ValueOf(&c).Elem().FieldByName(fieldName).Set(reflect.ValueOf(value))

	return c
}
