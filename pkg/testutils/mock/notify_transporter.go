package mock

// NotifyTransporter for testing
type NotifyTransporter struct {
	IsNotified bool
}

func (n *NotifyTransporter) Send(jsonData []byte) error {
	n.IsNotified = true
	return nil
}
