package router

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"samplecompany/pkg/dao"
	"samplecompany/pkg/dao/models"
	"samplecompany/pkg/testutils"
	"samplecompany/pkg/testutils/mock"

	"github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/require"
)

type testCase struct {
	Name       string
	C          models.Computer
	StatusCode int
}

func TestRouter(t *testing.T) {
	dbname := t.TempDir() + "/test.db"

	db, err := dao.New(dbname)
	require.NoError(t, err)

	r := New(db)

	numOfTestCases := 3
	testCaseComputers := testutils.GenFakeComputers(numOfTestCases)

	// TODO:
	// 1. Add test case for validating Abbreviation size constraint
	// 2. Add test case for non-allowed http methods

	t.Run("Create", func(t *testing.T) {
		tcs := []testCase{
			{
				Name:       "Computer-0",
				C:          testCaseComputers[0],
				StatusCode: http.StatusCreated,
			},
			{
				Name:       "Computer-1",
				C:          testCaseComputers[1],
				StatusCode: http.StatusCreated,
			},
			{
				Name:       "Computer-2",
				C:          testCaseComputers[2],
				StatusCode: http.StatusCreated,
			},
			{
				// OPTIONAL field
				Name:       "empty-Abbreviation",
				C:          testutils.GenFakeComputerWithField("Abbreviation", ""),
				StatusCode: http.StatusCreated,
			},
			{
				// OPTIONAL field
				Name:       "empty-Description",
				C:          testutils.GenFakeComputerWithField("Description", ""),
				StatusCode: http.StatusCreated,
			},
			{
				Name:       "empty-MAC",
				C:          testutils.GenFakeComputerWithField("MAC", ""),
				StatusCode: http.StatusBadRequest, // NOT NULL CONSTRAINT
			},
			{
				Name:       "empty-Name",
				C:          testutils.GenFakeComputerWithField("Name", ""),
				StatusCode: http.StatusBadRequest, // NOT NULL CONSTRAINT
			},
			{
				Name:       "empty-IPAddress",
				C:          testutils.GenFakeComputerWithField("IPAddress", ""),
				StatusCode: http.StatusBadRequest, // NOT NULL CONSTRAINT
			},
			{
				Name:       "Re-Create-Computer-2",
				C:          testCaseComputers[2],
				StatusCode: http.StatusBadRequest,
			},
		}
		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				body := requestWithJSON(t, r, tc.C, "/computer", tc.StatusCode)
				require.NotEmpty(t, body)
			})

		}
	})

	t.Run("Get", func(t *testing.T) {
		tcs := []testCase{
			{
				Name:       "Computer-0",
				C:          testCaseComputers[0],
				StatusCode: http.StatusOK,
			},
			{
				Name:       "Computer-1",
				C:          testCaseComputers[1],
				StatusCode: http.StatusOK,
			},
			{
				Name:       "Computer-2",
				C:          testCaseComputers[2],
				StatusCode: http.StatusOK,
			},
			{
				Name:       "Non-existing",
				C:          testutils.GenFakeComputer(), // Newly created
				StatusCode: http.StatusBadRequest,       // NOT CREATED
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				url := fmt.Sprintf("/computer/%s", tc.C.MAC)
				require.HTTPStatusCode(t, r.ServeHTTP, http.MethodGet, url, nil, tc.StatusCode)
			})
		}
	})

	t.Run("GetAll", func(t *testing.T) {
		computers, err := db.GetAllComputers()
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "/computer/all", nil)
		require.NoError(t, err)

		recorder := httptest.NewRecorder()
		r.ServeHTTP(recorder, req)

		gotComputer := []models.Computer{}

		err = json.NewDecoder(recorder.Body).Decode(&gotComputer)
		require.NoError(t, err)

		require.ElementsMatch(t, computers, gotComputer)
	})

	// TODO:
	// Add tests for getting all computers assigned to employee, endpoint '/computer/all/{abbr}'

	// Remove a computer from an employee
	t.Run("RemoveFromEmployee", func(t *testing.T) {
		// TODO: Add more test cases for
		// 1. Delete single item two times
		// 2. Delete item which doest not exist in the DB
		// 3. Get deleted items

		for _, c := range testCaseComputers {
			url := fmt.Sprintf("/computer/%s/remove/%s", c.MAC, c.Abbreviation)
			require.HTTPSuccess(t, r.ServeHTTP, http.MethodPost, url, nil)
		}
	})

	// Assign a computer to another employee
	t.Run("AssignToAnotherEmployee", func(t *testing.T) {
		// TODO: Add more test cases for
		// 1. Update items which doest not exist in the DB
		// 2. Updated the REQUIRED field to empty

		for i := range testCaseComputers {
			// Update employee
			testCaseComputers[i].Abbreviation = gofakeit.Lexify("???")

			data, err := json.Marshal(testCaseComputers[i])
			require.NoError(t, err)

			url := fmt.Sprintf("/computer/%s/assign/%s", testCaseComputers[i].MAC, testCaseComputers[i].Abbreviation)
			requestWithBody(t, r, bytes.NewReader(data), url, http.StatusOK)

			// Get back the updated computer and compare
			url = fmt.Sprintf("/computer/%s", testCaseComputers[i].MAC)
			require.HTTPBodyContains(t, r.ServeHTTP, http.MethodGet, url, nil, string(data))
		}
	})

	t.Run("Notification", func(t *testing.T) {
		// mock notification transporter
		tr := &mock.NotifyTransporter{}
		models.Notifier.SetTransporter(tr)

		tcs := testutils.GenFakeComputers(models.NotificationUsersLimit)
		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				tc.Abbreviation = "abc" // Same for all
				requestWithJSON(t, r, tc, "/computer", http.StatusCreated)
			})
		}

		// Check if `tr.IsNotified` is eventually set to true
		require.Eventually(t, func() bool { return tr.IsNotified }, 5*time.Second, 100*time.Millisecond)
	})
}

func requestWithJSON(t *testing.T, r http.Handler, comp models.Computer, url string, code int) []byte {
	buf := bytes.NewBuffer(nil)
	err := json.NewEncoder(buf).Encode(comp)
	require.NoError(t, err)

	return requestWithBody(t, r, buf, url, code)
}

func requestWithBody(t *testing.T, r http.Handler, body io.Reader, url string, code int) []byte {
	req, err := http.NewRequest(http.MethodPost, url, body)
	require.NoError(t, err)

	recorder := httptest.NewRecorder()
	r.ServeHTTP(recorder, req)

	require.Equal(t, code, recorder.Code)
	return recorder.Body.Bytes()
}
