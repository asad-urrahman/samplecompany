package router

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"samplecompany/pkg/dao"
	"samplecompany/pkg/dao/models"

	"github.com/gorilla/mux"
)

type handler struct {
	dao *dao.DAO
}

func New(dao *dao.DAO) http.Handler {
	h := handler{dao: dao}

	r := mux.NewRouter()
	// Create new computer
	r.HandleFunc("/computer", h.CreateComputer).Methods(http.MethodPost)
	// Get all computers in database
	r.HandleFunc("/computer/all", h.GetAllComputers).Methods(http.MethodGet)
	// Get all assigned computers to employee
	r.HandleFunc("/computer/all/{abbr}", h.GetEmployeeComputers).Methods(http.MethodGet)
	// Get computer identified by MAC address
	r.HandleFunc("/computer/{mac}", h.GetComputer).Methods(http.MethodGet)
	// Remove a computer from an employee
	r.HandleFunc("/computer/{mac}/remove/{abbr}", h.RemoveEmployeeComputer).Methods(http.MethodPost)
	// Assign a computer to another employee
	r.HandleFunc("/computer/{mac}/assign/{abbr}", h.AssignEmployeeComputer).Methods(http.MethodPost)
	return r
}

func (h *handler) CreateComputer(w http.ResponseWriter, r *http.Request) {
	c := &models.Computer{}
	err := json.NewDecoder(r.Body).Decode(c)
	if err != nil {
		// TODO: add logger
		http.Error(w, "Invalid computer data", http.StatusBadRequest)
		return
	}

	// TODO:
	// Validate fields

	err = h.dao.CreateComputer(c)
	if err != nil {
		// TODO: add logger

		// FIXME: Use checkErrorAs()
		// errors.As(err, &sqlite3.Error{Code: 19}) /* Abort due to constraint violation */
		// For details: http://www.sqlite.org/c3ref/c_abort.html
		// Use generic error, the sqlite3.Error{} limits the use of SQLite driver only

		checkErrorString(w, err, http.StatusBadRequest, "NOT NULL constraint failed", "UNIQUE constraint failed")
		return
	}

	responseJSON(w, http.StatusCreated, c) // Status created with body
}

func (h *handler) GetAllComputers(w http.ResponseWriter, r *http.Request) {
	computers, err := h.dao.GetAllComputers()
	if err != nil {
		// TODO: add logger
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	responseJSON(w, http.StatusOK, computers)
}

func (h *handler) GetComputer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	computer, err := h.dao.GetComputer(vars["mac"])
	if err != nil {
		// TODO: add logger
		// FIXME: Use checkErrorAs()
		checkErrorString(w, err, http.StatusBadRequest, "record not found")
		return
	}

	responseJSON(w, http.StatusOK, computer)
}

func (h *handler) GetEmployeeComputers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	computers, err := h.dao.GetEmployeeComputers(vars["abbr"])
	if err != nil {
		// TODO: add logger
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	responseJSON(w, http.StatusOK, computers)
}

func (h *handler) RemoveEmployeeComputer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	err := h.dao.RemoveEmployeeComputer(vars["abbr"], vars["mac"])
	if err != nil {
		// TODO: add logger
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func (h *handler) AssignEmployeeComputer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	err := h.dao.AssignEmployeeComputer(vars["abbr"], vars["mac"])
	if err != nil {
		// TODO: add logger
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func responseJSON(w http.ResponseWriter, code int, v interface{}) {
	w.WriteHeader(code)

	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		// TODO: add logger
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

}

func checkErrorAs(w http.ResponseWriter, err error, target any, code int) {
	if errors.As(err, target) {
		http.Error(w, "Invalid computer data", code)
		return
	}

	http.Error(w, "Internal error", http.StatusInternalServerError)
}

func checkErrorString(w http.ResponseWriter, err error, code int, msgs ...string) {
	for _, msg := range msgs {
		if strings.Contains(err.Error(), msg) {
			http.Error(w, "Invalid request data", http.StatusBadRequest)
			return
		}
	}

	// Something went wrong with DB
	http.Error(w, "Internal error", http.StatusInternalServerError)
}
