//go:build integration

package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"

	"samplecompany/pkg/dao/models"
	"samplecompany/pkg/testutils"

	"github.com/stretchr/testify/require"
)

// TODO:
// Automate this process

// Terminal 1. Run app
// cd cmd go build -o samplecompany . && ./samplecompany -port=9090
// Terminal 2. Run docker
// docker run --net=host greenbone/exercise-admin-notification
// Terminal 3. Run tests
// go test -tags=integration -v -count=1 .

func TestNotification(t *testing.T) {
	testCaseComputers := testutils.GenFakeComputers(models.NotificationUsersLimit + 1)

	for _, c := range testCaseComputers {
		c.Abbreviation = "aaa" // same for all

		buf := bytes.NewBuffer(nil)
		err := json.NewEncoder(buf).Encode(c)
		require.NoError(t, err)

		resp, err := http.Post("http://127.0.0.1:9090/computer", "application/json", buf)
		require.NoError(t, err)

		require.Equal(t, http.StatusCreated, resp.StatusCode)
	}
}
